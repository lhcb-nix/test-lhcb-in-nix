#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "from Configurables import MessageSvc" > test_gaudi_options.py
echo "from Gaudi.Configuration import VERBOSE" >> test_gaudi_options.py
echo "MessageSvc().OutputLevel = VERBOSE" >> test_gaudi_options.py
gaudirun.py test_gaudi_options.py
