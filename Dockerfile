FROM ubuntu

RUN apt-get update && apt-get install -y bzip2 curl locales && \
    locale-gen en_US.UTF-8 && \
    useradd --create-home nixuser && \
    mkdir -p -m 0755 /cvmfs/lhcbdev.cern.ch/nix && \
    chown nixuser /cvmfs/lhcbdev.cern.ch/nix
USER nixuser
ENV USER=nixuser
WORKDIR /home/nixuser

RUN curl -LO https://chrisburr.me/lhcb-nix-2.0/nix-2.0-2018_03_20-x86_64-linux.tar.bz2 && \
    curl https://chrisburr.me/lhcb-nix-2.0/install | bash
ENV LC_ALL="en_US.utf-8" \
    LANG="en_US.utf-8"

RUN . /home/nixuser/.nix-profile/etc/profile.d/nix.sh && \
    nix-env -i less gnugrep bash-interactive binutils vim nix-prefetch-scripts -j4 && \
    nix-env -i -f ~/.nix-defexpr/channels/nixpkgs/default.nix -A git -A man  -j4

ENV LHCB_NIX_ENV_DIR="/home/nixuser/.nix-profile"
ENV PATH="${LHCB_NIX_ENV_DIR}/bin:${LHCB_NIX_ENV_DIR}/sbin:${PATH}" \
    CMAKE_PREFIX_PATH="${LHCB_NIX_ENV_DIR}" \
    NIX_SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt" \
    SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt" \
    SHELL="/home/nixuser/.nix-profile/bin/bash"

ENTRYPOINT "/home/nixuser/.nix-profile/bin/bash"


# Install Gaudi
RUN nix-env -i gaudi-v29r3 -j4
COPY test_gaudi_launch.sh ${HOME}

# Temporary stuff to make Gaudi work
ENV PYTHONPATH="/cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/python:/cvmfs/lhcbdev.cern.ch/nix/store/r3hk4p3ap7gpw0f1bybdp1n3dlc647ba-root-6.12.06/lib:${PYTHONPATH}" \
    PATH="/cvmfs/lhcbdev.cern.ch/nix/store/isa547yip5jfbci638c8xm67w9xgbcxd-python-2.7.14-env/bin/:/cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/scripts:${PATH}" \
    LD_LIBRARY_PATH="/cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/lib" \
    ROOT_INCLUDE_PATH="/cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/include:${ROOT_INCLUDE_PATH}"

RUN chmod +w /cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/lib && \
    for fn in /cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/lib/lib*.so; do \
        listcomponents.exe $fn >> "${fn%.so}.components"; \
    done && \
    chmod -w /cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/lib


# Install LHCb
# error: Packages '/cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/manifest.xml' and
# '/cvmfs/lhcbdev.cern.ch/nix/store/0nfsy3q8mxgrwk7s5mbd1jwamn4j6gmn-gaudi-v29r3/manifest.xml' have the same priority '5'
# use 'nix-env --set-flag priority NUMBER INSTALLED_PKGNAME' to change the priority of one of the conflicting packages
# ('0' being the highest priority)
RUN nix-env --set-flag priority 9 gaudi-v29r3 && \
    nix-env -i LHCb-v44r0 -j4

# Temporary stuff to make LHCb work
ENV PYTHONPATH="/cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/python:${PYTHONPATH}" \
    PATH="/cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/scripts:${PATH}" \
    LD_LIBRARY_PATH="/cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/lib:${LD_LIBRARY_PATH}" \
    ROOT_INCLUDE_PATH="/cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/include:${ROOT_INCLUDE_PATH}"

RUN chmod +w /cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/lib && \
    for fn in /cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/lib/lib*.so; do \
        listcomponents.exe $fn >> "${fn%.so}.components"; \
    done && \
    chmod -w /cvmfs/lhcbdev.cern.ch/nix/store/r4bgzlxk692ybq089j6ryls5wj6afhhl-LHCb-v44r0/lib


# Install Lbcom
RUN nix-env --set-flag priority 8 LHCb-v44r0 && \
    nix-env -i Lbcom-v22r0 -j4

# Temporary stuff to make Lbcom work
ENV PYTHONPATH="/cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/python:${PYTHONPATH}" \
    PATH="/cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/scripts:${PATH}" \
    LD_LIBRARY_PATH="/cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/lib:${LD_LIBRARY_PATH}" \
    ROOT_INCLUDE_PATH="/cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/include:${ROOT_INCLUDE_PATH}"

RUN chmod +w /cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/lib && \
    for fn in /cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/lib/lib*.so; do \
        listcomponents.exe $fn >> "${fn%.so}.components"; \
    done && \
    chmod -w /cvmfs/lhcbdev.cern.ch/nix/store/3p2ywxwfn2izjw5x92gna9f655qg89ym-Lbcom-v22r0/lib
